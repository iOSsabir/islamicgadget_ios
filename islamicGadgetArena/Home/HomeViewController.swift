

import UIKit
import StoreKit
import Firebase
import CoreLocation
import UserNotifications


var lat = UserDefaults.standard.double(forKey: "lat")
var lon = UserDefaults.standard.double(forKey: "lon")



class HomeViewController: UIViewController  , UITableViewDelegate , UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    
    
    var locationManager = CLLocationManager()
    //var year : String = {return self.getYearForApi()}()
   // var month : String = {return self.getMonthForApi()}()
    
   // let url = "http://api.aladhan.com/v1/calendar?latitude=51.508515&longitude=-0.1254872&method=2&month=\(month)&year=\(year)"

    
    var counter = 0
    
    
    @IBOutlet weak var weatherImage: UIImageView!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var sidepanelView: UIView!
    
    var indexNo : Int?
    
    @IBOutlet weak var profilePic: UIImageView!
    
    
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var HizriLbl: UILabel!
    
    @IBOutlet weak var collectionView1: UICollectionView!
    
    @IBOutlet weak var collectionView2: UICollectionView!
    
    
    @IBOutlet weak var salatNamelbl: UILabel!
    
    
    @IBOutlet weak var salatStartingTimeLbl: UILabel!
    
    @IBOutlet weak var salatTimeLblofFirstCollectionView: UILabel!
    
    
//    var helloWorldTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(HomeViewController.sayHello), userInfo: nil, repeats: true)

//          @objc func sayHello()
//          {
//              NSLog("hello World")
//          }
    
    
    var arr = ["Login/Signup" , "Notification" , "Rating" ,"Share"  ]
    var salatName = ["Fajr","Dohur","Asr","Magrib","Isha"]
    var ContentImageSet = [UIImage(named:"kalima.png"),UIImage(named:"salat.png"),UIImage(named:"ramadan.png") ,UIImage(named:"hajj.png"),UIImage(named:"zakat.png"),UIImage(named:"more.png")]
    var salatTimeArr = [String]()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeViewController.sayHello), userInfo: nil, repeats: true)
        
        SalatTime()
        weatherCondition()
        print(UserDefaults.standard.double(forKey: "lat"))
        print(UserDefaults.standard.double(forKey: "lon"))
        
        
        //print(year , month)
        
        

       InternetCheck()
       //locations()


       sidepanelView.isHidden = true


       profilePic.isHidden = true
       profilePic.layer.cornerRadius = 10

       profilePic.clipsToBounds = true

       checkIfUserIsSignedIn()

        print("Auth.auth().currentUser : ",Auth.auth().currentUser?.uid)

        let time = currentTime()
        let gregorianDate = gregorian()
        let hijriDate = hijri()

        timeLbl.text = time
        dateLbl.text = gregorianDate
        HizriLbl.text = hijriDate
        
        
        
        if Auth.auth().currentUser != nil{

            
             arr = [ "Notification"  , "Rating" , "Share" , "Logout" ]
            self.tblView.reloadData()
DispatchQueue.main.async {
                self.tblView.reloadData()
          }
        }
        else{
            
             arr = ["Login/Signup" , "Notification" , "Rating" , "Share" , "Logout" ]

            self.tblView.reloadData()
           DispatchQueue.main.async {
                self.tblView.reloadData()
           }
        }
        
        
    }
    
    @objc func sayHello()
    {
        NSLog("hello World")
        var time = currentTime()
        print(time)
        timeLbl.text = time
        var splitedArr = time.components(separatedBy: " ")
        print(splitedArr[0])
        
        var timeInFloat = (splitedArr[0] as NSString).doubleValue
        var salatToPray = salatTimeChecking(condition: timeInFloat)
        print(salatToPray)
        salatNamelbl.text = salatToPray
    
        
    }
    
    
    func InternetCheck(){
        let networkCheck = isConnectedToInternet()
        
        if networkCheck == false{
            AlertControl.showAlert(self, title: "No Internet", message: "you need to enable internet to see the right salat time")
        }
    }
    
    
    func salatTimeChecking(condition: Double) -> String {
           
       switch (condition) {
       
       case 00.01...1.00 :
        
               return "Midnight"
           
       case 1.01...4.00 :
               return "Tahajjud"
           
       case 4.01...6.00 :
        salatStartingTimeLbl.text = "\(salatTimeArr[0])"
               return "Fajr"
           
       case 6.01...9.15 :
               return "Ishraq"
           
       case 9.16...12.00 :
               return "chast"
           
       case 12.01...15.00 :
        salatStartingTimeLbl.text = "\(salatTimeArr[1])"
               return "duhur"
           
       case 15.01...18.00 :
        salatStartingTimeLbl.text = "\(salatTimeArr[2])"
               return "asr"
           
       case 18.01...19.30 :
        salatStartingTimeLbl.text = "\(salatTimeArr[3])"
               return "Magrib"
           
       case 19.31...24.00 :
        salatStartingTimeLbl.text = "\(salatTimeArr[4])"
               return "Isha"
           
          
           
           default :
               return "Salat Time"
           }

       }
    
    
    
    func SalatTime(){
        
        URLSession.shared.dataTask(with: URL(string: "http://api.aladhan.com/v1/calendar?latitude=23.6850&longitude=90.3563&method=2&month=\(month)&year=\(year)")!) { (data, response, error) in
            
            guard let data = data else{
                return
            }
            
            let post = try! JSONDecoder().decode(Salat.self,from:data)
            print(post.data[0].timings)
            let salatNames = post.data[0].timings
            print(type(of: salatNames))
            
            let ins = post.data[0].timings
            
            let mirror = Mirror(reflecting: ins)
            
            for child in mirror.children{
                print(child.label! , child.value)
                self.salatTimeArr.append(child.value as! String)
            }
            
            
            DispatchQueue.main.async {
                print(self.salatTimeArr)
                 self.collectionView1.reloadData()
            }
            
            
        }.resume()
    }

    
    
    func weatherCondition(){

        let API = "http://api.openweathermap.org/data/2.5/weather?lat=23.6850&lon=90.3563&appid=e1a5bc43ba74c4e01acc53fff3aa3a40"

        URLSession.shared.dataTask(with: URL(string: API)!) { (data, response, error) in

               guard let data = data else{
                   return
               }

               let weatherConditionData = try! JSONDecoder().decode(Weather.self,from:data)
               var weatherCode = weatherConditionData.weather[0].id
            
            
            DispatchQueue.main.async {
                self.weatherImage.image = UIImage(named: updateWeatherIcon(condition: weatherCode))
            }
               
            print(weatherConditionData.weather[0].id)
            
            

           }.resume()
        
    }
    
    
    
    func hijri() -> String{
        let hijri = Calendar(identifier: .islamic)
        

        let formatter = DateFormatter()
        formatter.calendar = hijri
        formatter.dateFormat = "d MMMM yyyy"

        let today = Date()
        let dateString = formatter.string(from: today)

        print(dateString)
        
        return dateString
    }
    
    
    func gregorian() -> String{
        let date = Calendar(identifier: .gregorian)
        

        let formatter = DateFormatter()
        formatter.calendar = date
        formatter.dateFormat = "E d MMM "

        let today = Date()
        let dateString = formatter.string(from: today)

        print(dateString)
        
        return dateString
    }
    
    
    func currentTime() -> String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH mm a"
        //formatter.timeStyle = .short
        let dateTimeString = formatter.string(from: currentDateTime)
        return dateTimeString
    }
    func locations(){
        
        locationManager.requestWhenInUseAuthorization()
        var currentLoc: CLLocation!
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
        CLLocationManager.authorizationStatus() == .authorizedAlways) {
           currentLoc = locationManager.location
           print(currentLoc.coordinate.latitude)
           print(currentLoc.coordinate.longitude)
        }
        
    }
    
    
    func isConnectedToInternet() -> Bool {
    let hostname = "google.com"
    //let hostinfo = gethostbyname(hostname)
    let hostinfo = gethostbyname2(hostname, AF_INET6)//AF_INET6
    if hostinfo != nil {
        return true // internet available
      }
     return false // no internet
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! customTableViewCell
        cell.CellLbl.text = arr[indexPath.row]
        return cell
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        indexNo = indexPath.row
        
        
        print(indexNo!)
        
        
        if arr[indexNo!] == "Login/Signup" {
            
            
            performSegue(withIdentifier: "hola", sender: self)
        }
        
        
        
        if arr[indexNo!] == "Logout"{
            do{
                try Auth.auth().signOut()
            }catch{
                AlertControl.showAlert(self, title: "Something went wrong", message: "Didn't Logout successfully")
            }
        }
        
        
        if arr[indexNo!] == "Share"{
            let activityVc = UIActivityViewController(activityItems: ["https://apps.apple.com/ai/app/scanner-for-pdf/id1487613318"], applicationActivities: nil)
            activityVc.popoverPresentationController?.sourceView = self.view
            self.present(activityVc, animated: true, completion: nil)
            
            
        }
        
        if arr[indexNo!] == "Rating"{
            
//            let uiImagePicker = UIImagePickerController()
//                   uiImagePicker.allowsEditing = true
//                   uiImagePicker.sourceType = .photoLibrary
            SKStoreReviewController.requestReview()
        }
        
        
        if arr[indexNo!] == "Notification"{
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Notification", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            newViewController.modalPresentationStyle = .overFullScreen
            self.present(newViewController, animated: true, completion: nil)
        }
        
        
    }
    
    
    @IBAction func upperBtn(_ sender: Any) {
        tblView.reloadData()
        
        counter = counter + 1
        
        print(counter)
        
        if counter%2 != 0{
            
            self.sidepanelView.isHidden = false
        }
        
        else {
            self.sidepanelView.isHidden = true
        }
        
        
    }
    
    
    private func checkIfUserIsSignedIn() {

        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                self.arr = [ "Notification" , "Terms & Condition" , "Rating" , "Share" , "Logout" ]
                           
                           DispatchQueue.main.async {
                               self.tblView.reloadData()
                           }
                
                self.profilePic.isHighlighted = false
                
                // user is signed in
                // go to feature controller
            } else {
                
                self.arr = ["Login/Signup" , "Notification" , "Language" , "Terms & Condition" , "Rating" , "Share" ]
                
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
                 // user is not signed in
                 // go to login controller
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionView1{
            return salatName.count

        }
        
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        //salatCell.salatNameLB.text = salatName[indexPath.row]
        if collectionView == collectionView1{
            let salatCell = collectionView.dequeueReusableCell(withReuseIdentifier: "salattimeCell", for: indexPath) as! salattimeCollectionViewCell
            salatCell.salatTimeLB.text = salatName[indexPath.row]
            
                   return salatCell
        }else{
            let contentcell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCell", for: indexPath)as! contentCollectionViewCell
            
            contentcell.contentImageView.image = ContentImageSet[indexPath.row]
            return contentcell
            
        }
        
        
        
       
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionView1{
            let width = (collectionView.frame.width - 40) / 5
                   let height = width
                   print("width : ",width)
                   return CGSize(width: width, height: height)
        }else{
            let width = (collectionView.frame.width - 40) / 3
                   let height = width
                   print("width : ",width)
                   return CGSize(width: width, height: height)
        }
        
       
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "kalimaSegue" {
//
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
        if collectionView == collectionView2{
            if indexPath.row == 0{
                    performSegue(withIdentifier: "kalimaSegue", sender: nil)
                       
                   }
                   else if indexPath.row == 1{
                       performSegue(withIdentifier: "salat", sender: nil)
                       print("salat")
                   }
                   else if indexPath.row == 2{
                
                performSegue(withIdentifier: "ramadan", sender: nil)
                       print("ramadan")
                   }
                   else if indexPath.row == 3{
                       print("hajj")
                        performSegue(withIdentifier: "hajjSegue", sender: nil)
                   }
                   else if indexPath.row == 4{
                       print("zakat")
                    performSegue(withIdentifier: "zakatSegue", sender: self)
                   }
                   else if indexPath.row == 5{
                performSegue(withIdentifier: "moreSegue", sender: self)
                       print("more")
                   }
                   
        }
        
    }
    
    
}


