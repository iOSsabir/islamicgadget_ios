//
//  salattimeCollectionViewCell.swift
//  islamicGadgetArena
//
//  Created by Macbook on 01/03/2020.
//  Copyright © 2020 Shahid Sabir. All rights reserved.
//

import UIKit

class salattimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var salatTimeLB: UILabel!
    
    @IBOutlet weak var salatNameLB: UILabel!
    @IBOutlet weak var salatTimeview: UIView!
    
    override func awakeFromNib() {
           super.awakeFromNib()
        
        
//        salatTimeview.layer.cornerRadius = salatTimeview.frame.width / 2
        salatTimeview.layer.cornerRadius = 63.8/2
       }
    
}
